import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;

public class CreateAccountCorrectEmailTest {
    WebDriver driver;

    @BeforeTest
    public  void preCondition(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.get("http://automationpractice.com/index.php?controller=authentication&back=my-account");
    }

    @Test
    public void enterCorrectEmail() {
        driver.findElement(By.id("email_create")).sendKeys("ivanov@yahoo.com");
        driver.findElement(By.id("SubmitCreate")).click();
    }

    @AfterTest
    public void afterTest(){
        driver.close();
    }
}
